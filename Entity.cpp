//
// Created by vgalin on 13/06/2020.
//

#include "Entity.h"

#include <iostream> //TODO remove when no more debug needed

void Entity::takeTurn()  {

    //take turn if an ai object is attached to the current entity
    if(ai) {
        ai->takeTurn();
    }

    // starve if no food and random
    if (foodLevel <= 0 && rand() % 4 == 0) {
        takeDamage(1);
    }

    if (health <= 0) {
        deathFunction();
    }

    if (type == ghost_fly && rand() % 10001 == 0) {
        type = ENTITY_T::fly;
        health = 50;
        sprite.setColor(sf::Color(255, 255, 255, 255));

    }

}

void Entity::starve(int amount) {
    foodLevel -= amount;
}

void Entity::eat(Entity* target) {
    target->takeDamage(10);
    foodLevel += 10;
}

void Entity::takeDamage(int damageAmount) {

    health -= damageAmount;
}

void Entity::deathFunction() {

    if (type == ENTITY_T::fly) {
        type = ENTITY_T::ghost_fly;
        sprite.setColor(sf::Color(255, 255, 255, 100));
    } else if (type == ENTITY_T::go_home_pheromone || type == ENTITY_T::go_food_pheromone){
        delete this;
    } else {
        ai = NULL;
    }



    // todo delete THIS from entity list
}

void Entity::draw() {
    RenderWindow& w = RenderWindow::getInstance();
    sprite.setPosition(x * w.getSpriteSize(), y * w.getSpriteSize()); //TODO replace constants idk how atm (they are sprite size)
    w.screen->draw(sprite);
}

bool Entity::move(int dX, int dY) {
    if(!map->isBlocked(dX + x, dY + y)) {
        x += dX;
        y += dY;
    }
    return map->isBlocked(dX + x, dY + y);
}

// move to a target entity
bool Entity::move(Entity *target) {
    if (target) { // check that target is not a nullptr
        int dX = target->x - x;
        int dY = target->y - y;
        move(dX, dY);
    }
    return true;
}

void Entity::spreadPheromone(ENTITY_T type) {
    map->placeEntity(x, y, type);
}

Entity::~Entity() {
    map->deleteEntity(this);
    delete ai;
//        map->entities->erase(this);
}
