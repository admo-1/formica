//
// Created by vgalin on 12/06/2020.
//

#ifndef FORMICA_RENDERWINDOW_H
#define FORMICA_RENDERWINDOW_H

#include <SFML/Graphics.hpp>

// singleton class

class RenderWindow {
    public:
        static RenderWindow& getInstance()
        {
            static RenderWindow instance;
            return instance;
        }
        sf::RenderWindow* screen; //TODO move this into private + getter, maybe ?

        int value; // for debuging purposes

        int getSpriteSize() {return spriteSize;};
        void setSpriteSize(int _spriteSize) {spriteSize = _spriteSize;};

    private:
        int spriteSize;
        RenderWindow() {}
    public:
        RenderWindow(RenderWindow const&)    = delete;
        void operator=(RenderWindow const&)  = delete;
};


#endif //FORMICA_RENDERWINDOW_H
