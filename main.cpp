#include <iostream>
#include "Universe.h"


#include <ctype.h>
#include <stdio.h>
#include <unistd.h>

void help() {
    std::cout << "Some help info..." << std::endl;
    ::exit(0);
}

int main(int argc, char **argv) {

    // universe numbers
    int mapSize = 60;
    int spriteSize = 16;
    int consoleMode = false;
    char * seed = nullptr;
    int fps = 20;
    bool displayPheromones = false;

    // warning : very basic parameter parsing, can break very easily (ex : negative, two flags following each other ...)

    for(int i = 1; i < argc; i++) {
        std::cout << "argv: " << argv[i] << std::endl;

        if (strcmp(argv[i], "--map-size") == 0 || strcmp(argv[i], "-m") == 0) {
            if (i + 1 >= argc) {
                std::cout << "Please specify a map size after --map-size or -m";
                return 1;
            }
            mapSize = std::stoi(argv[i+1]);
        } else if (strcmp(argv[i], "--sprite-size") == 0 || strcmp(argv[i], "-n") == 0) {
            if (i + 1 >= argc) {
                std::cout << "Please specify a sprite size after --sprite-size or -n";
                return 1;
            }
            spriteSize = std::stoi(argv[i+1]);
        } else if (strcmp(argv[i], "--seed") == 0 || strcmp(argv[i], "-s") == 0) {
            if (i + 1 >= argc) {
                std::cout << "Please specify a seed --seed or -s";
                return 1;
            }
            seed = argv[i+1];
        } else if (strcmp(argv[i], "--fps") == 0 || strcmp(argv[i], "-f") == 0) {
            if (i + 1 >= argc) {
                std::cout << "Please specify an int after --fps or -f";
                return 1;
            }
            fps = std::stoi(argv[i + 1]);
        }  else if (strcmp(argv[i], "--pheromone") == 0 || strcmp(argv[i], "-p") == 0) {
            if (i + 1 >= argc) {
                std::cout << "Please specify an int after --pheromone or -p";
                return 1;
            }
            displayPheromones = strcmp(argv[i+1], "true") ? true : false;
        }
    }

    Universe u(mapSize, spriteSize, false, seed, fps, displayPheromones);
    u.mainLoop();

    return 0;
}

