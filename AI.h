//
// Created by moham on 16/06/2020.
//

#ifndef FORMICA_AI_H
#define FORMICA_AI_H
#include <iostream>
#include <vector>
#include "Enums.h"

class Entity;

class AI{
public:
    AI() {};
    virtual void takeTurn() = 0;
};


// does't do anything
class Soulless: public AI{
public:
    Soulless(Entity* _entity): entity(_entity) {};
    void takeTurn() override{}; // does nothing
    private:
        Entity* entity;
};

// wanders aimlessly and endlessly
class Wanderer: public AI{
    public:
        Wanderer(Entity* _entity): entity(_entity) {};
        void takeTurn() override;
    private:
        Entity* entity;
};

// wanders aimlessly and eventually dies of starvation
class HungryWanderer: public AI{
    public:
        HungryWanderer(Entity* _entity): entity(_entity) {};
        void takeTurn() override;
    private:
        Entity* entity;
};

// used for pheromones, takes damage each turn
class PheromoneAI: public AI{
    public:
        PheromoneAI(Entity* _entity): entity(_entity) {};
        void takeTurn() override;
    private:
        Entity* entity;
};


enum AntGoal {
    findHome,       // seeks go_home OR home pheromones
    findFood,       // spreads go_home pheromones, seeks go_food pheromones
    bringBackFood   // spreads go_food pheromones, seeks go_home OR home pheromones
};
// basic AI for ants, follows pheromones trails
class AntAI: public AI{
    public:
        AntAI(Entity* _entity): entity(_entity) {};
        void takeTurn() override;
    private:
        AntGoal goal = findHome;
        Entity* entity;

        int turnsUntilLost = 100;

        std::vector<Entity *> getNearby(ENTITY_T type);

        Entity *strongest(std::vector<Entity *> pheromoneList);

        Entity *weakest(std::vector<Entity *> pheromoneList);
};


#endif //FORMICA_AI_H
