//
// Created by moham on 30/05/2020.
//

#include "Universe.h"
#include "GuiRenderer.h"
#include "ConsoleRenderer.h"
#include "Enums.h";
#include <iostream>
#include <string.h>

Universe::Universe(int _mapSize, int _spriteSize, bool consoleMode, char* _seed, int _fps, bool _displayPheromones):
mapSize(_mapSize), spriteSize(_spriteSize), userSeed(_seed), maxFPS(_fps), displayPheromones(_displayPheromones){

    if (userSeed == nullptr) {
        intSeed = (unsigned)time(NULL); //seed used for random number generation (thus generating the map)
        std::cout << "User gave no seed, choosing one: " << intSeed << std::endl;
    } else {
        //djb2 hash algorithm to obtain an int from a string
        unsigned int hash = 5381;
        for (size_t i = 0; i < sizeof(userSeed); ++i)
            hash = 33 * hash + (unsigned char)userSeed[i];

        intSeed = hash;

        std::cout << "User gave seed: " << userSeed << std::endl;
        std::cout << "Seed is translated to : " << intSeed << std::endl;

    }
    srand(intSeed); //using seed as seed for random number generation

    map = new Map(_mapSize, _mapSize, intSeed);


    RenderWindow& w = RenderWindow::getInstance();  // RenderWindow singleton
    w.setSpriteSize(spriteSize);

    if (consoleMode) {
        renderer = new ConsoleRenderer();
    } else {
        renderer = new GuiRenderer(_mapSize, _mapSize, displayPheromones);
    }
}

float getFPS(const sf::Time& time) {
    return (1000000.0f / time.asMicroseconds());
}

void Universe::mainLoop() {

    int fpsDisplayRate;
    sf::Clock FPSClock;

    if (maxFPS < 0) {
        fpsDisplayRate = 5000;
    } else {
        fpsDisplayRate = maxFPS * 5;
    }

    bool displayFPS = true;

    RenderWindow& w = RenderWindow::getInstance();  // RenderWindow singleton
    w.screen->setFramerateLimit(maxFPS); // max fps = 5

    std::cout << "Entering main loop \n";

    int i = 0;

    while (w.screen->isOpen()) {

        // handle events
        sf::Event event;
        while (w.screen->pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
                w.screen->close();

            if (event.type == sf::Event::MouseButtonPressed) {
                int x = event.mouseButton.x;
                int y = event.mouseButton.y;

                if (event.mouseButton.button == sf::Mouse::Right) {
                    map->becomeWall(x/w.getSpriteSize(), y/w.getSpriteSize());
                }

                if (event.mouseButton.button == sf::Mouse::Left) {
                    map->becomeGrass(x/w.getSpriteSize(), y/w.getSpriteSize());
                }

                if (event.mouseButton.button == sf::Mouse::Middle) {
                    map->placeEntity(x/w.getSpriteSize(), y/w.getSpriteSize(), ENTITY_T::ant);
                }
            }
        }

        takeTurn();

        renderer->clear();
        renderer->drawMap(*map);
        renderer->drawEntities(*map);
        renderer->update();

        map->buildEntityMap();

        float fps = getFPS(FPSClock.restart());

        // print the FPS count is maxFPX is not set to unlimited
        if (i%fpsDisplayRate == 0 && displayFPS) {
            std::cout << "FPS : "<< fps << "\n";
        }

        i++;
    }

    std::cout << "Quitting ...\n";
}

void Universe::takeTurn() {
    map->takeTurn();
    turn++;
}
