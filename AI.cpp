//
// Created by moham on 16/06/2020.
//

#include "AI.h"
#include "Entity.h"


void Wanderer::takeTurn(){
    // move randomly and doesn't interract with anything
    entity->move(
        std::rand() % 3 - 1,
        std::rand() % 3 - 1
    );
};


void HungryWanderer::takeTurn(){
    // move randomly and doesn't interract with anything
    entity->move(
        std::rand() % 3 - 1,
        std::rand() % 3 - 1
    );

    entity->starve(1);
};


void PheromoneAI::takeTurn() {
    entity->takeDamage(1);
}




std::vector<Entity *> AntAI::getNearby(ENTITY_T type) {
    return entity->map->getNearbyEntities(entity->x, entity->y, type);
}

Entity* AntAI::strongest(std::vector<Entity *> pheromoneList) {

    int highestHealth = 0;
    Entity* strongest = nullptr;

    for (auto pheromone: pheromoneList) {
        if(pheromone->getHealth() > highestHealth) {
            highestHealth = pheromone->getHealth();
            strongest = pheromone;
        }
    }

    return strongest;
}

Entity* AntAI::weakest(std::vector<Entity *> pheromoneList) {

    int lowerHealth = 999;
    Entity* weakest = nullptr;

    for (auto pheromone: pheromoneList) {
        if(pheromone->getHealth() < lowerHealth) {
            lowerHealth = pheromone->getHealth();
            weakest = pheromone;
        }
    }

    return weakest;
}

void AntAI::takeTurn(){

    //enum AntGoal {
//    findHome,       // seeks go_home OR home pheromones
//    findFood,       // spreads go_home pheromones, seeks go_food pheromones
//    bringBackFood   // spreads go_food pheromones, seeks go_home OR home pheromones
//};


    if (goal == findHome) {
        // search home neaby
//        std::cout << "goal find home \n";

        std::vector<Entity *> nearbyHomes = getNearby(home_pheromone);

        if (!nearbyHomes.empty()) { // if home goto home and change goal
            entity->move(nearbyHomes[0]);
//            turnsUntilLost = 600;
            goal = findFood;

        } else { // else move randomly or approximatively towards home

            if(std::rand()%5 == 0) {
                int dX = entity->x < entity->map->getWidth()/2 ? 1:-1;
                int dY = entity->y < entity->map->getHeight()/2 ? 1:-1;
                if (!entity->move(dX, dY)) {
                    while (entity->move(std::rand() % 3 - 1, std::rand() % 3 - 1));
                }
            } else {
                while (entity->move(std::rand() % 3 - 1, std::rand() % 3 - 1));

            }

        }

    } else if (goal == findFood) {

        entity->spreadPheromone(go_home_pheromone);

        std::vector<Entity *> nearbyFlies = getNearby(fly);
        std::vector<Entity *> nearbyFoodPath = getNearby(go_food_pheromone);

        if (!nearbyFlies.empty()) {
            entity->move(nearbyFlies[0]);
            entity->eat(nearbyFlies[0]);

            goal = bringBackFood;

        } else if (!nearbyFoodPath.empty() && std::rand()%3 == 0) {
            Entity* target = weakest(nearbyFoodPath);
            entity->move(target);

        } else {
            while (entity->move(std::rand() % 3 - 1, std::rand() % 3 - 1));
        }


    } else if (goal == bringBackFood) {

        entity->spreadPheromone(go_food_pheromone);


        std::vector<Entity *> nearbyHomes = getNearby(home_pheromone);
        std::vector<Entity *> nearbyHomePath = getNearby(go_home_pheromone);

        if (!nearbyHomes.empty()) {
            entity->move(nearbyHomes[0]);

            goal = findFood;
        } else if (!nearbyHomePath.empty() && std::rand()%3 == 0){
            Entity* target = weakest(nearbyHomePath);
            entity->move(target);

        }else {
             // else move randomly or approximatively towards home
            if(std::rand()%5 == 0) {
                int dX = entity->x < entity->map->getWidth()/2 ? 1:-1;
                int dY = entity->y < entity->map->getHeight()/2 ? 1:-1;

                if (!entity->move(dX, dY)) {
                    while (entity->move(std::rand() % 3 - 1, std::rand() % 3 - 1));
                }
            } else {
                while (entity->move(std::rand() % 3 - 1, std::rand() % 3 - 1));
            }
        }
    }

//    if (nearbyEntities.size() != 0) {
//        std::cout << "seeing: " << nearbyEntities.size() << " entity" << std::endl;
//    }





    //entity->starve(1);
};