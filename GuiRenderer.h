//
// Created by zegre on 12/06/2020.
//

#ifndef FORMICA_GUIRENDERER_H
#define FORMICA_GUIRENDERER_H

#include "Tilemap.h"
#include "Map.h"
#include <SFML/Graphics.hpp>

#include "Renderer.h"

class GuiRenderer : public Renderer {

public:
    GuiRenderer(int mapWidth, int mapHeight, bool _displayPheromones);
    void drawMap(Map &map);
    void drawEntities(Map &map);
    void clear();
    void update();

private:

    int spriteSize; // height == width size of the sprites in the tilemap
    char* tilesetFile ;

    TileMap* tileMap;

    int screenWidth;
    int screenHeight;
    bool displayPheromones;
};


#endif //FORMICA_GUIRENDERER_H
