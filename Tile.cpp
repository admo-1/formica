//
// Created by moham on 30/05/2020.
//

#include "Tile.h"

Tile::Tile() {
    blocked = false;
    repr = ' ';
    type = TILE_T::grass;
}

Tile &Tile::operator=(const Tile &t) {
    if(this != &t)
    {
        blocked = t.blocked;
        repr = t.repr;
    }
    return *this;
}

bool Tile::isBlocked() {
    return blocked;
}

void Tile::becomeWall() {
    blocked = true;
    repr = (char)254u; //■
    type = TILE_T::wall;
}

void Tile::becomeHome() {
    blocked = false;
    repr = 'H';
    type = TILE_T::home;
}

void Tile::becomeGrass() {
    blocked = false;
    repr = ' ';
    type = TILE_T::grass;
}