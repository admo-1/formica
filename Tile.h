//
// Created by moham on 30/05/2020.
//

#ifndef FORMICA_TILE_H
#define FORMICA_TILE_H

#include <iostream>
#include "Enums.h"


class Tile {
private:
    bool blocked;

public:

    Tile();
    char repr; // console repr
    TILE_T type; //
    Tile& operator =(const Tile& t);

    bool isBlocked();

    void becomeWall();
    void becomeHome();
    void becomeGrass();
};


#endif //FORMICA_TILE_H
