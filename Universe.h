//
// Created by moham on 30/05/2020.
//

#ifndef FORMICA_UNIVERSE_H
#define FORMICA_UNIVERSE_H


#include "Map.h"
#include "Renderer.h"


//const int SIZE_X = 120; //width
//const int SIZE_Y = SIZE_X; //height
class Universe {
public:
    Universe(int _mapSize, int _spriteSize, bool consoleMode, char* _seed, int _fps, bool _displayPheromones);
    void mainLoop();

private:
    int mapSize;
    int spriteSize;
    int maxFPS;

    int turn = 0;

    bool displayPheromones;

    char* userSeed; // seed given by the user (nullptr or string)
    unsigned int intSeed; //seed translated from the user string

    Map* map;
    Renderer* renderer;

    void takeTurn();
};


#endif //FORMICA_UNIVERSE_H
