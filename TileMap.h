//
// Created by vgalin on 12/06/2020.
//

// NB : the code in this file is coming from the SFML documentation
// https://www.sfml-dev.org/tutorials/2.5/graphics-vertex-array.php

#ifndef FORMICA_TILEMAP_H
#define FORMICA_TILEMAP_H


#include <SFML/Graphics.hpp>

class TileMap : public sf::Drawable, public sf::Transformable
{
public:

    bool loadTilesetFromfile(const std::string& tileset);

    bool load(sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height);

private:

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    sf::VertexArray m_vertices;
    sf::Texture m_tileset;
};


#endif //FORMICA_TILEMAP_H
