//
// Created by zegre on 12/06/2020.
//

#ifndef FORMICA_CONSOLERENDERER_H
#define FORMICA_CONSOLERENDERER_H

#include "Renderer.h"

class ConsoleRenderer : public Renderer {
public:
    ConsoleRenderer();
    void drawMap(Map &map);
    void drawEntities(Map &map) {};

    void clear() {};
    void update() {};

private:

};

#endif //FORMICA_CONSOLERENDERER_H
