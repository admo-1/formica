//
// Created by vgalin on 20/06/2020.
//

#ifndef FORMICA_RESOURCEMANAGER_H
#define FORMICA_RESOURCEMANAGER_H

#include <SFML/Graphics.hpp>
#include "Enums.h"

// singleton class
class ResourceManager {

    public:
        static ResourceManager& getInstance()
        {
            static ResourceManager instance;
            return instance;
        }

        sf::Texture* dispenceTexture(ENTITY_T type) {
            if (type == ENTITY_T::ant) {
                return &antTexture;
            } else if (type == ENTITY_T::fly) {
                return &flyTexture;

            } else if (type == ENTITY_T::go_home_pheromone) {
                return &goHomePhTexture;
            } else if (type == ENTITY_T::go_food_pheromone) {
                return &goFoodPhTexture;
            } else if (type == ENTITY_T::home_pheromone) {
                return &homePhTexture;
            }
            return &defaultTexture;
        }

        int value; // for debuging purposes

    private:
        ResourceManager() {
            antTexture.loadFromFile("ant.png");
            flyTexture.loadFromFile("fly.png");
            defaultTexture.loadFromFile("default.png");

            goHomePhTexture.loadFromFile("go_home_pheromone.png");
            goFoodPhTexture.loadFromFile("go_food_pheromone.png");
            homePhTexture.loadFromFile("home_pheromone.png");
        }

        sf::Texture defaultTexture;
        sf::Texture antTexture;
        sf::Texture flyTexture;

        sf::Texture goHomePhTexture;
        sf::Texture goFoodPhTexture;
        sf::Texture homePhTexture;

    public:
        ResourceManager(ResourceManager const&) = delete;
        void operator=(ResourceManager const&)  = delete;
};



#endif //FORMICA_RESOURCEMANAGER_H
