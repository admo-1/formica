//
// Created by moham on 30/05/2020.
//

#include "Map.h"
#include <iostream> //TODO REMOVE
#include "Enums.h"
#include "CellularAutomata.h"


#include <stdlib.h>
#include <time.h>

Tile* Map::operator[](const int x) {
    return tiles[x];
}

Map::Map(int _width, int _height, unsigned int _seed) {
    width = _width;
    height = _height;
    unsigned int seed = _seed;

    //init array
    tiles = new Tile*[height];

    for (int i = 0; i < height; ++i) {
      tiles[i] = new Tile[width];
    }

    // init map
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            tiles[i][j] = *(new Tile());
        }
    }

    generateTerrain();
    populateMap();


    // init lines of the entity map
    entityMap = new std::vector<Entity*>*[height];

    // inits content of the entity map
    for (int i = 0; i < height; ++i) {
        entityMap[i] = new std::vector<Entity*>[width];
    }

}

void Map::generateTerrain() {
    int nbWalls = (height * width) / 4;

    // use a cellular automata to generate a "cave-like" map
    int** automataMap = generateWithAutomata(width, height, seed);

    // place blocks and flies using the automata-generated map
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if (automataMap[i][j] == 1) {
                tiles[i][j].becomeWall();
            } else if (automataMap[i][j] == 2) {
                placeEntity(i, j, ENTITY_T::fly);
            }
        }
    }

    // find the center of the map
    int centerX = width / 2;
    int centerY = height / 2;

    // radius of the anthill
    int radius = 2;

    // generate the anthill
    for (int i = -radius; i < radius; ++i) {
        for (int j = -radius; j < radius; ++j) {
            tiles[centerY + i][centerX + j].becomeHome();
            placeEntity(centerY + i, centerX + j, ENTITY_T::home_pheromone);
        }
    }
}

void Map::populateMap() {

    int centerX = width / 2;
    int centerY = height / 2;

    int nbAnt = 25;

    for (int j = 0; j < nbAnt; ++j) {
        placeEntity(centerX, centerY, ENTITY_T::ant);
    }

}

Map::Map(const Map &m) {
    for(int i = 0; i < m.height; i++)
    {
        for(int j = 0; j < m.width; j++)
        {
            tiles[i][j] = m.tiles[i][j];
        }
    }
}

Map &Map::operator=(const Map &m) {
    if(this != &m)
    {
        for(int i = 0; i < m.height; i++)
        {
            for(int j = 0; j < m.width; j++)
            {
                tiles[i][j] = m.tiles[i][j];
            }
        }
    }
    return *this;
}

//int** Map::getReprOLD() {
//
//    int** repr = 0;
//    repr = new int*[(unsigned int)height]; // weird crash if not using unsigned int
//
//    for (int h = 0; h < height; h++) {
//        repr[h] = new int[width];
//
//        for (int w = 0; w < width; w++) {
//            repr[h][w] = tiles[h][w].blocked ? 1:0;
//        }
//    }
//
//    return repr;
//}

int* Map::getReprConsole() {
    int* repr = 0;
    repr = new int[width * height];

    for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {
            repr[w + h * width] = tiles[h][w].repr;
        }
    }
    return repr;
}

int* Map::getReprGui() {

    int* repr = 0;
    repr = new int[width * height];

    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            repr[j + i * width] = tiles[i][j].type;
        }
    }



    return repr;
}

int Map::getWidth() {
    return width;
}

int Map::getHeight() {
    return height;
}

void Map::print_map() {

//    std::cout << "RENDER : " << width << " * " << height << "\n";
//
//    for(int x = 0; x < height; x++) {
//        for(int y = 0; y < width; y++) {
//
//            char c;
//
//            if (tiles[x][y].blocked) {
//                c = 'X';
//            } else {
//                c = '.';
//            }
//            std::cout << c;
//        }
//        std::cout << '\n';
//    }
}


bool Map::isBlocked(int x, int y) {
    return tiles[x][y].isBlocked();
}

void Map::takeTurn() {
    for(int i = 0; i < entityCount; i++) {
        entities[i]->takeTurn();
    }
}

void Map::placeEntity(int x, int y, ENTITY_T type){
    Entity* entity = new Entity(x, y, this, type);
    entities.push_back(entity);
    entityCount++;
}

std::vector<Entity *> Map::getNearbyEntities(int x, int y, ENTITY_T filter) {

    std::vector<Entity *> nearbyEntities;

    int radius = 1;

    for (int i = -radius; i <= radius; ++i) {
        for (int j = -radius; j <= radius; ++j) {
            std::vector<Entity*>::iterator entity;
            for(entity = entityMap[x + i][y + j].begin(); entity != entityMap[x + i][y + j].end(); ++entity) {
                if((*entity)->type == filter && i!= 0 && j!= 0) {
                    nearbyEntities.push_back(*entity);
                }
            }
        }
    }


    return nearbyEntities;

}


void Map::buildEntityMap() {

    // init the 2d array with empty vectors
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            entityMap[i][j] = std::vector<Entity *>();
        }
    }

    std::vector<Entity*>::iterator entity;
    for(entity = entities.begin(); entity != entities.end(); ++entity) {
        int x = (*entity)->x;
        int y = (*entity)->y;
        entityMap[x][y].push_back(*entity);
    }
}

void Map::deleteEntity(Entity* target) {
    entities.erase(std::remove(entities.begin(), entities.end(), target), entities.end());
    entityCount--;
}