//
// Created by zegre on 21/06/2020.
//

#ifndef FORMICA_ENUMS_H
#define FORMICA_ENUMS_H

// entity types
enum ENTITY_T{ant, fly, ghost_fly, unknown, go_home_pheromone, go_food_pheromone, home_pheromone};

// tile types
// enum type to match a type of texture with its index on the tileset file
enum TILE_T{wall, grass, home, blank};

// 8-wind compass rose + origin position
enum DIRECTION_T{N, NE, E, SE, S, SW, W, NW, ORIGIN};


#endif //FORMICA_ENUMS_H
