//
// Created by moham on 30/05/2020.
//

#ifndef FORMICA_MAP_H
#define FORMICA_MAP_H

#include <iostream>
#include <vector>
#include "Entity.h"
#include "Tile.h"

class Entity;
class Map {
private:
    Tile** tiles;

    int width = -1;
    int height = -1;
    unsigned long seed; //map seed

    void generateTerrain();
    void populateMap();


public:
    Map(int _width, int _height, unsigned int _seed);
    Map(const Map& m);

    std::vector<Entity*> entities;
    int entityCount = 0;

//    Entity*** entityMap;
    std::vector<Entity*>** entityMap; //dynamic 2d array of vectors

    Tile* operator[](const int x);
    Map& operator=(const Map& m);

    int** getReprOLD();
    int* getReprConsole();
    int* getReprGui();

    int getWidth();
    int getHeight();
    bool isBlocked(int x, int y);
    void takeTurn();

    void becomeWall(int x, int y) {
        tiles[x][y].becomeWall();
    }

    void becomeGrass(int x, int y) {
        tiles[x][y].becomeGrass();
    }

    void placeFly(int x, int y);

    void print_map(); // TODO remove (or keep for debugging ?)
//    void placePheromone(int x, int y);

    void placeEntity(int x, int y, ENTITY_T type);

    void buildEntityMap();


    std::vector<Entity*> getNearbyEntities(int x, int y, ENTITY_T filter);

    // get nearby tiles where "i" can move method
    void deleteEntity(Entity *target);
};


#endif //FORMICA_MAP_H
