//
// Created by vgalin on 13/06/2020.
//

#ifndef FORMICA_ENTITY_H
#define FORMICA_ENTITY_H

#include <SFML/Graphics.hpp>
#include "RenderWindow.h"
#include "ResourceManager.h"
#include "Map.h"
#include "Enums.h"
#include "AI.h"

class Map; // prevents circular import problem
class AI;


class Entity {
public:

    Entity(int _x, int _y, Map* _map, ENTITY_T _type=ENTITY_T::ant):
    x(_x), y(_y), map(_map), type(_type)
    {
        // get a texture depending of the entity type
        ResourceManager& rM = ResourceManager::getInstance();
        sf::Texture* texture = rM.dispenceTexture(type);
        sprite.setTexture(*texture);

        // get an AI depending on the entity type
        if(type == ENTITY_T::ant) {
            ai = new AntAI(this);
        } else if (type == ENTITY_T::go_home_pheromone || type == ENTITY_T::go_food_pheromone) {
            ai = new PheromoneAI(this);
        } else {
            ai = nullptr;
        }

        // todo get amount of life depending of entity

        health = 50;
        foodLevel = 100;

        if (type == ENTITY_T::go_home_pheromone || type == ENTITY_T::go_food_pheromone) {
            health = 250;
        }



    };

    ~Entity();

    int x, y;
    ENTITY_T type;

    Map* map;
    AI* ai;


    sf::Sprite getSprite() const {return sprite;}
    void draw();

    void takeTurn();
    void takeDamage(int damageAmount);

    void starve(int amount);
    void eat(Entity *target);

    void spreadPheromone(ENTITY_T type);

    void deathFunction();

    bool move(int x, int y);
    bool move(Entity* entity);

    int getHealth() {return health;};
private:

    sf::Sprite sprite;
    int health;
    int foodLevel;

};



#endif //FORMICA_ENTITY_H
