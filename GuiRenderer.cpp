//
// Created by vgalin on 12/06/2020.
//

#include "GuiRenderer.h"
#include "RenderWindow.h"
#include <iostream>


GuiRenderer::GuiRenderer(int mapWidth, int mapHeight, bool _displayPheromones):
displayPheromones(_displayPheromones) {

    RenderWindow& w = RenderWindow::getInstance();

    std::string tilesetFile = "tileset_16.png";

    if (w.getSpriteSize() == 8) {
        tilesetFile = "tileset_8.png";
    }


    tileMap = new TileMap();
    tileMap->loadTilesetFromfile(tilesetFile);

    screenWidth = mapWidth * w.getSpriteSize();
    screenHeight = mapHeight * w.getSpriteSize();


    w.screen = new sf::RenderWindow(sf::VideoMode(screenWidth, screenHeight), "Formica");

}

void GuiRenderer::drawMap(Map& map) {
    int* mapRepr = map.getReprGui();
    RenderWindow& w = RenderWindow::getInstance();

    if (!tileMap->load(sf::Vector2u(w.getSpriteSize(), w.getSpriteSize()), mapRepr, map.getWidth(), map.getHeight())) {
        exit(55);
    }

    w.screen->draw(*tileMap);
}

void GuiRenderer::drawEntities(Map& map) {

    std::vector<Entity*>::iterator entity;

    for(entity = map.entities.begin(); entity != map.entities.end(); ++entity) {
        if (!displayPheromones) {
            (*entity)->draw();
        } else if ((*entity)->type <  ENTITY_T::unknown) {
            (*entity)->draw();
        }
    }
}

void GuiRenderer::clear() {
    RenderWindow& w = RenderWindow::getInstance();
    w.screen->clear();
}

void GuiRenderer::update() {
    RenderWindow& w = RenderWindow::getInstance();
    w.screen->display();
}