//
// Created by zegre on 22/06/2020.
//

#ifndef FORMICA_CELLULARAUTOMATA_H
#define FORMICA_CELLULARAUTOMATA_H

#include <iostream>
#include <stdlib.h>
#include <time.h>


// inspired from
// https://gamedevelopment.tutsplus.com/tutorials/generate-random-cave-levels-using-cellular-automata--gamedev-9664

int chanceToStartAlive = 40;
int birthLimit = 4;
int deathLimit = 3;
int numberOfSteps = 2; //2

int countAliveNeighbours(int** map, int x, int y, int width, int height) {
    int count = 0;

    for(int i=-1; i<2; i++){
        for(int j=-1; j<2; j++){
            int neighbour_x = x+i;
            int neighbour_y = y+j;
            //If we're looking at the middle point
            if(i == 0 && j == 0){
                //Do nothing, we don't want to add ourselves in!
            }
            //In case the index we're looking at it off the edge of the map
            else if(neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= height || neighbour_y >= width){
                count = count + 1;
            }
            //Otherwise, a normal check of the neighbour
            else if(map[neighbour_x][neighbour_y] == 1){
                count = count + 1;
            }
        }
    }

    return count;
}


int** doSimulationStep(int** oldMap, int width, int height) {
    int** newMap = new int*[height];
    for(int i = 0; i < height; ++i)
        newMap[i] = new int[width];

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            newMap[i][j] = 0;
        }
    }
    //...

    //Loop over each row and column of the map
    for(int x=0; x < height; x++){
        for(int y=0; y< width; y++){
            int nbs = countAliveNeighbours(oldMap, x, y, width, height);
            //The new value is based on our simulation rules
            //First, if a cell is alive but has too few neighbours, kill it.
            if(oldMap[x][y] == 1){
                if(nbs < deathLimit){
                    newMap[x][y] = 0;
                }
                else{
                    newMap[x][y] = 1;
                }
            } //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
            else{
                if(nbs > birthLimit){
                    newMap[x][y] = 1;
                }
                else{
                    newMap[x][y] = 0;
                }
            }
        }
    }
    return newMap;
}

int** initialiseMap(int** map, int width, int height) {

    //random noise
    for (int i = 0; i < height; ++i)
        for (int j = 0; j < width; ++j) {
            if(std::rand() % 101 < chanceToStartAlive)
                map[i][j] = 1;
        }

    return map; // needed ?
}

int** placeTreasures(int** map, int width, int height) {
    //How hidden does a spot need to be for treasure?
    //I find 5 or 6 is good. 6 for very rare treasure.
    int treasureHiddenLimit = 5;
    for (int x = 0; x < height; x++){
        for (int y = 0; y < width; y++){
            if(map[x][y] == 0){
                int nbs = countAliveNeighbours(map, x, y, width, height);
                if(nbs >= treasureHiddenLimit){
                    map[x][y] = 2;
                }
            }
        }
    }

    return map;
}

int** generateWithAutomata(int width, int height, unsigned int seed) {

    int** map;

    map = new int*[height];
    for(int i = 0; i < height; ++i)
        map[i] = new int[width];

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            map[i][j] = 0;
        }
    }



    map = initialiseMap(map, width, height);

    //highway through noise
    for(int x = 0; x < height; x++) {
        map[x][width/2] = 0;
        map[x][width/2 + 1] = 0;
    }

    //highway through noise
    for(int y = 0; y < width; y++) {
        map[height/2][y] = 0;
        map[height/2 + 1][y] = 0;
    }
    //highway through noise (diagonals
    for(int x = 0; x < height; x++) {
        map[x][x] = 0;
        map[x][x+1] = 0;

        map[x][height - x] = 0;
        map[x][height - x + 1 ] = 0;
    }




    for (int i = 0; i < numberOfSteps; i++){
        map = doSimulationStep(map, width, height);
    }

    //place walls around the map
    for(int x = 0; x < height; x++) {
        map[x][0] = 1;
        map[x][width - 1] = 1;
    }
    //place walls around the map
    for(int y = 0; y < width; y++) {
        map[0][y] = 1;
        map[height - 1][y] = 1;
    }

    map = placeTreasures(map, width, height);

    return map;
    //print map
//    for (int i = 0; i < height; ++i) {
//        for (int j = 0; j < width; ++j) {
//            std::cout << map[i][j];
//        }
//        std::cout << std::endl;
//    }

}


#endif //FORMICA_CELLULARAUTOMATA_H
