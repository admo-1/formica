//
// Created by vgalin on 11/06/2020.
//

#ifndef FORMICA_RENDERER_H
#define FORMICA_RENDERER_H

#include "Tilemap.h"
#include "Map.h"
#include <SFML/Graphics.hpp>

class Renderer {
public:
    Renderer() {};

    virtual void drawMap(Map& map) = 0;
    virtual void drawEntities(Map& map) = 0;
//    func render logs (?)

    virtual void clear() = 0;
    virtual void update() = 0;



private:
    int levelWidth;
    int levelHeight;
};


#endif //FORMICA_RENDERER_H
